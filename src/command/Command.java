package command;

import characters.Person;

public interface Command {
	
	public void execute(Person c);

}
