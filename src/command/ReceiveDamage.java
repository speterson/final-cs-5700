package command;

import weapons.Weapon;
import characters.Person;



public class ReceiveDamage implements Command {

	Weapon w;
	public ReceiveDamage(Weapon w){
		this.w = w;
	}
	@Override
	public void execute(Person p) {
		p.getVitals().subtract(w.getVitals());;
	}

}
