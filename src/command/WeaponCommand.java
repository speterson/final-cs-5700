package command;


import weapons.Weapon;
import characters.Person;

public class WeaponCommand implements Command{
	
	private Weapon weapon;
	
	public WeaponCommand(Weapon weapon)
	{
		this.weapon = weapon;
	}
	
	public void execute(Person receiver)
	{
		receiver.getVitals().subtract(weapon.getVitals());
	}


	
	@Override
	public String toString()
	{
		return weapon.toString();
	}

}
