package weapons;

import characters.Vitals;

public class Wand extends Weapon {


	public Wand() {
		super("Wand", new Vitals(25, 15, 8));
	}
}
