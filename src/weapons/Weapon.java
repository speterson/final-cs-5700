package weapons;

import characters.Vitals;


public class Weapon {
	

	private Vitals vitals;
	private String name;
	
	public Weapon(String name, Vitals vitals)
	{
		this.name = name;
		this.vitals = vitals;
	}
	
	public Vitals getVitals()
	{
		return vitals;
	}
	
	public Weapon getReverse()
	{
		return new Weapon(name, vitals.getReverse());
	}
	
	public String getName()
	{
		return name + ": " + vitals;
	}

	@Override
	public String toString() {
		return name;
	}
	
}
