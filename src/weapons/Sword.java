package weapons;

import characters.Vitals;

public class Sword extends Weapon {

	public Sword() {
		super("Sword", new Vitals(30, 10, 5));
	}

}
