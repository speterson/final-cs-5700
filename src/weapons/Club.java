package weapons;

import characters.Vitals;

public class Club extends Weapon {


	public Club() {
		super("Club", new Vitals(40, -5, 5));
	}
}
