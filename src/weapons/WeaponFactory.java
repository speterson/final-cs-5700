package weapons;

public class WeaponFactory {

	public Weapon getWeapon(String weapon){
		if(weapon == null){
			return null;
		}
		if(weapon.equalsIgnoreCase("fireball")){
			return new Fireball();
		}
		else if(weapon.equalsIgnoreCase("club")){
			return new Club();
		}
		else if(weapon.equalsIgnoreCase("wand")){
			return new Wand();
		}
		else if(weapon.equalsIgnoreCase("sword")){
			return new Sword();
		}
		return null;
	}
	
}
