package gui;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import weapons.WeaponFactory;
import application.GamePlay;
import characters.Enemy;
import characters.Person;

public class SimpleController{

	private GamePlay gp = new GamePlay();
	//private List<Weapon> attacks;
	private WeaponFactory wf = new WeaponFactory();
	private List<Label> players;
	private int currentLabel = 0;
	private int previousLabel = 2;
	
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label player3Label;

    @FXML
    private Label enemyNameLabel;

    @FXML
    private Button fireballButton;

    @FXML
    private Button swordButton;

    @FXML
    private Label player2Label;

    @FXML
    private Button clubButton;

    @FXML
    private Label player1Label;

    @FXML
    private Label queueLabel;

    @FXML
    private Label vitalsLabel;

    @FXML
    private Button endGameButton;

    @FXML
    private Button wandButton;

    @FXML
    private Button endTurnButton;

    @FXML
    private Label enemyVitalsLabel;

    @FXML
    private Label messageBoxLabel;

    @FXML
    private Button newGameButton;
    
    @FXML
    private Label levelLabel;
    
    @FXML
    void fc0606(ActionEvent event) {

    }

    @FXML
    void initialize() {
        assert player3Label != null : "fx:id=\"player3Label\" was not injected: check your FXML file 'Overview.fxml'.";
        assert enemyNameLabel != null : "fx:id=\"enemyNameLabel\" was not injected: check your FXML file 'Overview.fxml'.";
        assert fireballButton != null : "fx:id=\"fireballButton\" was not injected: check your FXML file 'Overview.fxml'.";
        assert swordButton != null : "fx:id=\"swordButton\" was not injected: check your FXML file 'Overview.fxml'.";
        assert player2Label != null : "fx:id=\"player2Label\" was not injected: check your FXML file 'Overview.fxml'.";
        assert clubButton != null : "fx:id=\"clubButton\" was not injected: check your FXML file 'Overview.fxml'.";
        assert player1Label != null : "fx:id=\"player1Label\" was not injected: check your FXML file 'Overview.fxml'.";
        assert queueLabel != null : "fx:id=\"queueLabel\" was not injected: check your FXML file 'Overview.fxml'.";
        assert vitalsLabel != null : "fx:id=\"vitalsLabel\" was not injected: check your FXML file 'Overview.fxml'.";
        assert endGameButton != null : "fx:id=\"endGameButton\" was not injected: check your FXML file 'Overview.fxml'.";
        assert wandButton != null : "fx:id=\"wandButton\" was not injected: check your FXML file 'Overview.fxml'.";
        assert endTurnButton != null : "fx:id=\"endTurnButton\" was not injected: check your FXML file 'Overview.fxml'.";
        assert enemyVitalsLabel != null : "fx:id=\"enemyVitalsLabel\" was not injected: check your FXML file 'Overview.fxml'.";
        assert messageBoxLabel != null : "fx:id=\"messageBoxLabel\" was not injected: check your FXML file 'Overview.fxml'.";
        assert newGameButton != null : "fx:id=\"newGameButton\" was not injected: check your FXML file 'Overview.fxml'.";
        assert levelLabel != null : "fx:id=\"levelLabel\" was not injected: check your FXML file 'Overview.fxml'.";
        
        startNewGame();
        
       // updateGuiPlayerInfo();
        
        
      /*  newGameButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               
            	startNewGame();
            	
                
            }

			
        });*/
        endGameButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
               
                disableScreen();
               // newGameButton.setVisible(true);
            }

			
        });
           clubButton.setOnAction(new EventHandler<ActionEvent>() {

               @Override
               public void handle(ActionEvent event) {
                   weaponButtonPressed("club");
               }


           });
           
           fireballButton.setOnAction(new EventHandler<ActionEvent>() {

               @Override
               public void handle(ActionEvent event) {
            	   weaponButtonPressed("fireball");
               }
           });
           
           wandButton.setOnAction(new EventHandler<ActionEvent>() {

               @Override
               public void handle(ActionEvent event) {
            	   weaponButtonPressed("wand");
               }
           });
           
           swordButton.setOnAction(new EventHandler<ActionEvent>() {

               @Override
               public void handle(ActionEvent event) {
            	   weaponButtonPressed("sword");
               }
           });
           
           endTurnButton.setOnAction(new EventHandler<ActionEvent>() {

               @Override
               public void handle(ActionEvent event) {
            	   if(!gp.getEndGame()){
            		   incrementCurrentPlayer();
            		   if(!players.isEmpty()){
            			   gp.eachTurn();
                    	   updateGuiPlayerInfo();
                    	   messageBoxLabel.setText(gp.getMessageString());
                    	   messageBoxLabel.setTextFill(Color.BLACK);
                    	   levelLabel.setText(gp.getLevel()+1+ "");
            		   }
            		   else{
            			   messageBoxLabel.setText("All Players have lost");
            			   disableScreen();
            			   endGameButton.setDisable(true);
            			//   newGameButton.setVisible(true);
            		   }
                	   
            	   }
            	   else{
            		   disableScreen();
            		   //newGameButton.setVisible(true);
            		   messageBoxLabel.setText("Game Over, You Win!");
            		   
            	   }
            	   
               }
           });
		
	}
    

	private void startNewGame() {
		messageBoxLabel.setText("");
        disableWeaponsButtons(false);
        newGameButton.setVisible(false);
        endTurnButton.setDisable(false);
        

        gp = new GamePlay();
        buildPlayerLabelList();
        displayVitals(gp.getCurrentPlayer(), gp.getCurrentEnemy());
        levelLabel.setText(gp.getLevel()+1 + "");
        updateGuiPlayerInfo();
		
	}

	private void buildPlayerLabelList() {
		players = new ArrayList<Label>();
		players.add(player1Label);
		players.add(player2Label);
		players.add(player3Label);
		
	}

	public void updateGuiPlayerInfo(){
       	players.get(currentLabel).setTextFill(Color.BLUE);
       	players.get(previousLabel).setTextFill(Color.BLACK);
       	
       	displayQueue(gp.getCurrentPlayer());
		displayVitals(gp.getCurrentPlayer(), gp.getCurrentEnemy());
       }
       
	private void disableScreen() {
		player1Label.setTextFill(Color.BLACK);
		player2Label.setTextFill(Color.BLACK);
		player3Label.setTextFill(Color.BLACK);
		
		vitalsLabel.setText("");
		queueLabel.setText("");
		messageBoxLabel.setText("Game Over!");
		endTurnButton.setDisable(true);
		
		disableWeaponsButtons(true);
			
	}
       

	private void disableWeaponsButtons(boolean value){
		clubButton.setDisable(value);
		wandButton.setDisable(value);
		swordButton.setDisable(value);
		fireballButton.setDisable(value);
    	   
	}
       
       
	private void displayVitals(Person p, Enemy e) {
       vitalsLabel.setText(p.getVitals().toString());
       enemyVitalsLabel.setText(e.getVitals().toString());
       enemyNameLabel.setText(e.getName());
	}
       
	private void weaponButtonPressed(String weapon) {
		if(gp.addWeapon(gp.getCurrentPlayer(), wf.getWeapon(weapon))){
			messageBoxLabel.setText("Weapon: " + weapon + " was added to your queue.");
		}
		else{
			messageBoxLabel.setTextFill(Color.RED);
			messageBoxLabel.setText("Weapon: " + weapon + " could not be added to your queue\n queue is full. Please end turn");
			
		}	
		displayQueue(gp.getCurrentPlayer());

	}
		
	private void displayQueue(Person p){
		queueLabel.setText(p.commandsSent());
	}
		
	private void incrementCurrentPlayer(){
		gp.incrementCurrentPlayer();
		if(!gp.checkIfPersonAlive(gp.getCurrentPlayer())){
			players.remove(currentLabel);
			System.out.println(players.size());
		}
		if(currentLabel >= players.size()-1){
			currentLabel = 0;
		}
		else
			currentLabel++;
		if(previousLabel >= players.size()-1){
			previousLabel = 0;
		}
		else
			previousLabel++;
	}
}

















