package application;

import java.util.Scanner;

import weapons.WeaponFactory;
import characters.EnemiesGenerator;
import characters.Enemy;
import characters.Person;
import characters.Vitals;

import command.Command;
import command.WeaponCommand;

public class Driver {
	private WeaponFactory wf = new WeaponFactory();
	private Person steve = new Person("Steve", new Vitals(100,100,100));
	private Enemy currentEnemy;
	private final int LEVEL1 = 2;
	private final int LEVEL2 = 4;
	private final int LEVEL3 = 6;
	private final int LEVEL4 = 10;
	
	public static void main(String[] args)
	{
		Driver d = new Driver();
		d.doDriver();	
	}
	
	private void doDriver()
	{
		Scanner a = new Scanner(System.in);
		
		String next = a.nextLine();
		
		EnemiesGenerator level1 = new EnemiesGenerator(LEVEL1);
		EnemiesGenerator level2 = new EnemiesGenerator(LEVEL2);
		EnemiesGenerator level3 = new EnemiesGenerator(LEVEL3);
		EnemiesGenerator level4 = new EnemiesGenerator(LEVEL4);
		
		currentEnemy = level1.createEnemyList();
		
		while(next.equals("quit") == false)
		{
			doActionBasedOnStringInput(next);
			next = a.nextLine();
		}
		
		a.close();
	}
	
	private void doActionBasedOnStringInput(String s)
	{
		if (s.equals("blah"))
		{
			System.out.println("Calling blah!");
		}
		if (s.equals("enemy1"))
		{
			Command c = new WeaponCommand(wf.getWeapon("club"));
			currentEnemy.receiveMessage(c);
			System.out.println(currentEnemy);
		}
		
		if (s.equals("enemy2"))
		{
			Command c = new WeaponCommand(wf.getWeapon("club"));
			currentEnemy.receiveMessage(c);
			System.out.println(currentEnemy);
		}
		if (s.equals("Club"))
		{
			Command c = new WeaponCommand(wf.getWeapon("club"));
			steve.receiveMessage(c);
			System.out.println(steve);
		}
		else if (s.equals("Sword"))
		{
			Command c = new WeaponCommand(wf.getWeapon("sword"));
			steve.receiveMessage(c);
			System.out.println(steve);
		}
		else if (s.equals("Fireball"))
		{
			Command c = new WeaponCommand(wf.getWeapon("fireball"));
			steve.receiveMessage(c);
			System.out.println(steve);
		}
		else if(s.equals("s list"))
		{
			Command c = new WeaponCommand(wf.getWeapon("club"));
			steve.sendMessage(c);
			System.out.println(steve.commandsSent());
		}


	}
}
