package application;

import java.util.ArrayList;
import java.util.List;

import weapons.Weapon;
import characters.EnemiesGenerator;
import characters.Enemy;
import characters.Person;
import characters.Vitals;

import command.Command;
import command.WeaponCommand;

public class GamePlay{
	
	//private Vitals vitals = new Vitals(100, 100, 100);
	private final int HEALTH = 100;
	private final int STAMINA = 100;
	private final int MAGICA = 100;
	private final int numberOfPlayers = 3;
	
	private List<Person> players;
	private volatile int currentPlayer;
	private EnemiesGenerator eg;
	private Enemy currentEnemy;
	
	private List<Integer> levels = new ArrayList<Integer>();
	
	private final int LEVEL1 = 0;
	private final int LEVEL2 = 1;
	private final int LEVEL3 = 2;
	private final int LEVEL4 = 4;
	
	private int level;
	
	private final int MAX_LEVEL = 3;
	private String messageString;
	
	//number of attacks that can be queued up at one time
	private int queueSize = 5;

	
	private volatile boolean endGame;
	private boolean win;
	
	public GamePlay(){
		level = 0;
		messageString = "";
		endGame = false;
		win = false;
		currentPlayer = 0;
		
		createPlayers(numberOfPlayers);
		createLevels();
		eg = new EnemiesGenerator(levels.get(level));
		currentEnemy = eg.createEnemyList();
		
	}
	
	private void createLevels() {
		levels.add(LEVEL1);
		levels.add(LEVEL2);
		levels.add(LEVEL3);
		levels.add(LEVEL4);
		
		
	}
	

	public void eachTurn(){
		if(!endGame){
			Person p = players.get(currentPlayer);
			enemyAttacksPerson(p);
			attackEnemy(p);
			if(checkIfPersonAlive(p)){
				if(!checkIfEnemyAlive(currentEnemy)){
					messageString = currentEnemy.getName() + " has died.";
					currentEnemy = currentEnemy.getNextEnemy();
					if(currentEnemy == null){
						levelUp();
					}
				}
				else{
				//	System.out.println("enemy is alive");
				}
			}
			else{
				messageString = "You have died";
			}
		}	
	}
	
	private void attackEnemy(Person p) {
		if(!p.getSendList().isEmpty()){
			Command c = p.getSendList().remove(0);
			currentEnemy.receiveMessage(c);
			messageString = "You attacked " + currentEnemy.getName() + " with a " + c;
		
		}
		else{
			messageString = "";
		}
	}

	public boolean checkIfPersonAlive(Person p) {
		return p.isAlive();
	}
	
	private boolean checkIfEnemyAlive(Enemy e){
		return e.isAlive();
	}
	
	public void enemyAttacksPerson(Person p){
		p.getVitals().subtract(currentEnemy.getWeaponVitals());
	}
	
	//returns false if level 4 has been reached and game is over
	public boolean levelUp(){
		if(level < MAX_LEVEL){
			level++;
			for(int i = 0; i < players.size(); i++){
				players.get(i).setVitals(new Vitals(HEALTH, STAMINA, MAGICA));
			}
			
			eg = new EnemiesGenerator(levels.get(level));
			currentEnemy = eg.createEnemyList();
			return true;
		}
		else{
			System.out.println("game over");
			endGame = true;
			setWin(true);
			return false;
		}
	}
	
	
	private void createPlayers(int numberOfPlayers){
		players = new ArrayList<Person>();
		for(int i = 1; i <= numberOfPlayers; i++){
			players.add(new Person("player" + i, new Vitals(HEALTH, STAMINA, MAGICA)));
			
		}
	}

	public Boolean getEndGame() {
		return endGame;
	}

	public void setEndGame(Boolean endGame) {
		this.endGame = endGame;
	}
	
	public int getLevel(){
		return level;
	}
	
	public String getMessageString(){
		return messageString;
	}
	public List<Person> getPlayers() {
		return players;
	}
	
	public void incrementCurrentPlayer(){
		if(currentPlayer >= numberOfPlayers -1){
			currentPlayer = 0;
		}
		else
			currentPlayer++;
	}
	
	public Person getCurrentPlayer(){
		return players.get(currentPlayer);
	}
	public void setPlayers(List<Person> players) {
		this.players = players;
	}
	public Enemy getCurrentEnemy() {
		return currentEnemy;
	}

	
	public boolean addWeapon(Person p, Weapon w){
		if(p.getSendList().size() < queueSize){
			p.sendMessage(new WeaponCommand(w));
			return true;
		}
		return false;
		
	}

	public boolean isWin() {
		return win;
	}

	public void setWin(boolean win) {
		this.win = win;
	}
	
	
	
	

}
