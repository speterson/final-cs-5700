package characters;

import java.util.ArrayList;
import java.util.List;

import command.WeaponCommand;

import weapons.Weapon;
import weapons.WeaponFactory;

public class Enemy extends Person {

	private List<Enemy> nextEnemy;
	private Weapon weapon;
	
	Enemy(String name, Vitals vitals, String weapon) {
		super(name, vitals);
		this.weapon = new WeaponFactory().getWeapon(weapon);
		nextEnemy = new ArrayList<Enemy>();
	}
	
	
	public void addEnemy(Enemy e){
		nextEnemy.add(e);
	}
	public Enemy getNextEnemy(){
		if(!nextEnemy.isEmpty()){
			//System.out.println("next enemy in list");
			return nextEnemy.remove(0);
		}
		return null;
		
	}
	
/*	public Boolean isAlive(){
		if(vitals.getHealth() >=0){
			return true;
		}
		return false;
	}*/
	
	public void attackPerson(){
		WeaponCommand w = new WeaponCommand(weapon);
		this.getSendList().add(w);
	}
	
	public Vitals getWeaponVitals(){
		return weapon.getVitals();
	}
}
