package characters;

import java.util.concurrent.ConcurrentLinkedQueue;

import command.Command;

public class Worker extends Thread {

	protected volatile boolean stop = false;
	Person owner;
	ConcurrentLinkedQueue<Command> list;
	
	public Worker(Person owner, ConcurrentLinkedQueue<Command> list){
		this.owner = owner;
		this.list = list;
	}


	@Override
	public void run() {
		while(!stop){
			Command c = list.poll();
			if (c != null) {
				c.execute(owner);
			}
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		

	}
	
	public void stopThread() throws InterruptedException{
		stop = true;
		//System.out.println("thread is going to stop now");
	}

}
