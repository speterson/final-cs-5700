package characters;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class EnemiesGenerator {
	
	private List<String> names;
	private List<String> weapons;
	private int numberOfEnemies;
	public EnemiesGenerator(int enemies){
		this.names = new ArrayList<String>();
		this.weapons = new ArrayList<String>();
		this.numberOfEnemies = enemies;
		generateNameList();
		generateWeaponList();
	}
	
	private void generateNameList(){
		names.add("Darth Vadar");
		names.add("Hulk");
		names.add("Voldemort");
		names.add("Gollum");
		names.add("Dracula");
		names.add("The Joker");
	}
	private void generateWeaponList(){
		weapons.add("club");
		weapons.add("fireball");
		weapons.add("wand");
		weapons.add("sword");
		
	}
	
	private final int MIN = 50;
	private final int MAX = 125;
	private final int MIN_NAMES = 0;
	private final int MAX_NAMES = 5;
	private final int MIN_WEAPON = 0;
	private final int MAX_WEAPON = 3;
	
	private int randomNumber(int min, int max){
		Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	public Enemy createEnemyList(){
		Enemy e = new Enemy(names.get(randomNumber(MIN_NAMES, MAX_NAMES)), new Vitals(randomNumber(MIN, MAX), randomNumber(MIN, MAX), randomNumber(MIN, MAX)), weapons.get(randomNumber(MIN_WEAPON, MAX_WEAPON)));
		
		for(int i = 0; i < numberOfEnemies; i++){
			e.addEnemy(new Enemy(names.get(randomNumber(MIN_NAMES, MAX_NAMES)), new Vitals(randomNumber(MIN, MAX), randomNumber(MIN, MAX), randomNumber(MIN, MAX)), weapons.get(randomNumber(MIN_WEAPON, MAX_WEAPON))));
		}
		return e;
	}

}
