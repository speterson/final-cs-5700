package characters;

public class Vitals {
	
	int health;
	int magika;
	int stamina;
	
	public Vitals(int health, int magika, int stamina)
	{
		this.health = health;
		this.magika = magika;
		this.stamina = stamina;
	}
	
	public void subtract(Vitals v)
	{
		health -= v.getHealth();
		magika -= v.getMagika();
		stamina -= v.getStamina();
	}
	
	public Vitals getReverse()
	{
		return new Vitals(health * -1, magika * -1, stamina * -1);
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getMagika() {
		return magika;
	}

	public void setMagika(int magika) {
		this.magika = magika;
	}

	public int getStamina() {
		return stamina;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;
	}
	
	@Override
	public String toString()
	{
		return "Health: " + health + "\nMagica: " + magika + "\nStamina: " + stamina;
	}
	
	

}
