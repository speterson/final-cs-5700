package characters;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import command.Command;

public class Person{
	protected Vitals vitals;
	protected String name;
	
	private List<Command> sendList = new LinkedList<Command>();
	protected ConcurrentLinkedQueue<Command> receiveList = new ConcurrentLinkedQueue<Command>(); 

	private Worker worker;


	
	
	public Person(String name, Vitals vitals)
	{
		this.name = name;
		this.vitals = vitals;
		this.worker = new Worker(this, receiveList);
		worker.start();

	}
	
	public void sendMessage(Command m){
		sendList.add(m);
	
	}
	
	public void receiveMessage(Command c)
	{
		receiveList.add(c);
	}
	
	
	@Override
	public String toString()
	{
		return "Name: " + name + "; " + vitals.toString();
	}

	public String getName()
	{
		return name;
	}
	
	public void setName(String s)
	{
		this.name = s;
	}
	
	
	public List<Command> getSendList() {
		return sendList;
	}

	public ConcurrentLinkedQueue<Command> getReceiveList() {
		return receiveList;
	}

	public Vitals getVitals(){
		return vitals;
	}
	
	public void setVitals(Vitals vitals){
		this.vitals = vitals;
	}
	public Boolean isAlive(){
		if(vitals.health >= 0){
			return true;
		}
		return false;
	}
	

	public String listOfSent(){
		return sendList.toString();
	}
	
	public String commandsSent(){
		{
			StringBuilder sb = new StringBuilder();
			
		//	sb.append("Commands to do:\n");
			Command[] list = sendList.toArray(new Command[0]);
			for (int i = 0; i < list.length; i++)
			{
				sb.append("\t" + list[i].toString());
				sb.append("\n");
			}
			
			
			return sb.toString();
		}
	}
	


}
